﻿var RULES = {};
RULES[3] = 1;
RULES[4] = 1;
RULES[5] = 2;
RULES[6] = 3;
RULES[7] = 5;
RULES[8] = 11;
app.controller("thirdTaskController", function ($scope, calculateService) {
    $scope.words = '';
    $scope.points = 0;

    $scope.calculatePoints = function () {
        $scope.points = calculateService.calculate($scope.words);
    };
});

// Service 
app.factory('calculateService', function () {
    return {
        calculate: function (words) {
            var removeWhitespaces = words.replace(/\s*,\s*/g, ",");
            var partsOfStr = removeWhitespaces.split(',');

            var points = 0;
            var i;
            for (i = 0; i < partsOfStr.length; i++) {
                if (partsOfStr[i].length > 8) {
                    points = points + RULES[8];
                } if (partsOfStr[i].length < 3) {
                    points = points + RULES[3];
                } else {
                    points = points + RULES[partsOfStr[i].length];
                }
            }
            return points;
        }
    }
});