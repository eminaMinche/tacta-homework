﻿var RULES = {};
RULES[3] = 1;
RULES[4] = 1;
RULES[5] = 2;
RULES[6] = 3;
RULES[7] = 5;
RULES[8] = 11;



// Controller
app.controller("secondTaskController", function ($scope, calculateService) {
    $scope.pointsForPlayers = [];
    $scope.players = 0;
    $scope.array = [];

    $scope.saveNumberOfPlayers = function () {
        $scope.array = [];
        $scope.pointsForPlayers = [];
        for (var k = 0; k < $scope.players; k++) {
            $scope.array.push({ player: '', number: k+1});
        }
    };
    
    $scope.calculatePointsForPlayers = function () {
        $scope.pointsForPlayers = calculateService.calculate($scope.array);
    };
});

// Service 
app.factory('calculateService', function () {
    return {
        calculate: function (arrayOfPlayers) {
            var u;
            pointsForPlayers = [];
            for (u = 0; u < arrayOfPlayers.length; u++) {
                var removeWhitespaces = arrayOfPlayers[u].player.replace(/\s*,\s*/g, ",");
                var partsOfStr = removeWhitespaces.split(',');

                var pointsForOnePlayer = 0;
                var i;
                for (i = 0; i < partsOfStr.length; i++) {
                    if (partsOfStr[i].length < 3) {
                        pointsForOnePlayer = pointsForOnePlayer + RULES[3];
                    } else if (partsOfStr[i].length > 8) {
                        pointsForOnePlayer = pointsForOnePlayer + RULES[8];
                    } else {
                        pointsForOnePlayer = pointsForOnePlayer + RULES[partsOfStr[i].length];
                    }
                }
                pointsForPlayers.push(pointsForOnePlayer);
            }
            return pointsForPlayers;
        }
    }
});