﻿var RULES = {};
RULES[3] = 1;
RULES[4] = 1;
RULES[5] = 2;
RULES[6] = 3;
RULES[7] = 5;
RULES[8] = 11;


// Controller
app.controller("firstTaskController", function ($scope, calculateServiceForOnePlayer) {
    $scope.words = '';
    $scope.pointsForPlayer = 0;

    $scope.calculatePoints = function () {
         $scope.pointsForPlayer = 0;
        $scope.pointsForPlayer = calculateServiceForOnePlayer.calculate($scope.words);
    };
});

// Service 
app.factory("calculateServiceForOnePlayer", function () {
    return {
        calculate: function (words) {
            var removeWhitespaces = words.replace(/\s*,\s*/g, ",");
            var partsOfStr = removeWhitespaces.split(',');

            var pointsForPlayer = 0;
            var i;
            for (i = 0; i < partsOfStr.length; i++) {
                if (partsOfStr[i].length > 8) {
                    pointsForPlayer = pointsForPlayer + RULES[8];
                } if (partsOfStr[i].length < 3) {
                    pointsForPlayer = pointsForPlayer + RULES[3];
                } else {
                    pointsForPlayer = pointsForPlayer + RULES[partsOfStr[i].length];
                }
            }
            return pointsForPlayer;
        }
    }
});